class CreatePorts < ActiveRecord::Migration
  def change
    create_table :ports do |t|
      t.string :ip, limit: 50
      t.string :descricao, limit: 100

      t.timestamps null: false
    end
  end
end
